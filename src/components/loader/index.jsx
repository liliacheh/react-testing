import style from "./loader.module.scss"

export function Loader (){
    return (
        <div className={style.gooey}>
            <span className={style.dot}></span>
            <div className={style.dots}>
                <span className={style.loaderSpan}></span>
                <span className={style.loaderSpan}></span>
                <span className={style.loaderSpan}></span>
            </div>
        </div>
        
    )
}