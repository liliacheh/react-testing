import { render, screen, fireEvent, cleanup } from '@testing-library/react';
import { Provider } from 'react-redux';
import Modal from '../modal/index.jsx';
import configureStore from 'redux-mock-store';
import { setModalType } from '../../redux/actions/modal';
import { setArt } from '../../redux/actions/article.js';

afterEach(cleanup)

const mockStore = configureStore([]);
const store = mockStore({
  modalType: { modalType: 'buy' },
  cart: [],
  article: 300125128,
  products: { products: [] },
});

test('should component exist', () => {
  const { container } = render(
    <Provider store={store}>
      <Modal />
    </Provider>
  );
  expect(container).toMatchSnapshot();
  expect(screen.getByTestId('modal')).toBeInTheDocument();
});

test('should close modal when close button is clicked', () => {
  store.dispatch = jest.fn();

  render(
    <Provider store={store}>
      <Modal />
    </Provider>
  );
  const closeButton = screen.getByTestId('close-button');
  fireEvent.click(closeButton);

  expect(store.dispatch).toHaveBeenCalledWith(setModalType(null));
  expect(store.dispatch).toHaveBeenCalledWith(setArt(null));
});

test('should add product to cart when "Add to cart" button is clicked', () => {
  store.dispatch = jest.fn();

  render(
    <Provider store={store}>
      <Modal />
    </Provider>
  );

  const addToCartButton = screen.getByText('Add to cart');
  fireEvent.click(addToCartButton);

  expect(store.dispatch).toHaveBeenCalledWith(expect.any(Function));
});
