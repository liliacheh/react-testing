import Button from "../button";
import { render, fireEvent, cleanup, screen } from '@testing-library/react';
import renderer from "react-test-renderer"
import '@testing-library/jest-dom/extend-expect';

afterEach(cleanup)
const onClickMock = jest.fn()
const btnProps = {
    className : 'button',
    text : 'Add to cart',
    onClick : onClickMock,
    title: 'Add product to cart',
    children : <span>Click me</span>
}
test('test basic props', () => {
    render(<Button {...btnProps} />);
    expect(screen.getByText('Add to cart')).toBeInTheDocument();
    expect(screen.getByTestId('button')).toHaveClass('button');
    expect(screen.getByTestId('button')).toHaveAttribute('title', 'Add product to cart');
    expect(screen.getByTestId('button')).toContainElement(document.querySelector('span'));
  })
  test('calls onClick function when button is clicked', () => {
    render(<Button {...btnProps} />)
    const button = screen.getByTestId('button');
    fireEvent.click(button);
    expect(onClickMock).toHaveBeenCalled()
  })
test('snapshot for component Button should exist', () => {
    const button = renderer.create(<Button {...btnProps}/>).toJSON;
    expect(button).toMatchSnapshot();
});




