import { render, screen, fireEvent, cleanup } from '@testing-library/react';
import { Provider } from 'react-redux';
import Modal from '../modal/index.jsx';
import configureStore from 'redux-mock-store';
import { setModalType } from '../../redux/actions/modal';
import { setArt } from '../../redux/actions/article.js';
import { removeProductFromCart } from '../../redux/actions/cart.js';

afterEach(cleanup);

const mockStore = configureStore([]);
const store = mockStore({
  modalType: { modalType: 'delete' },
  cart: [],
  article: 300125128,
  products: { products: [] },
});

test('should component exist', () => {
  const { container } = render(
    <Provider store={store}>
      <Modal />
    </Provider>
  );
  expect(screen.getByTestId('modal')).toBeInTheDocument();
  expect(container).toMatchSnapshot();
});

test('should close modal when "Cancel" button is clicked', () => {
  store.dispatch = jest.fn();

  render(
    <Provider store={store}>
      <Modal />
    </Provider>
  );
  const closeButton = screen.getByTestId('close-button');
  fireEvent.click(closeButton);

  expect(store.dispatch).toHaveBeenCalledWith(setModalType(null));
  expect(store.dispatch).toHaveBeenCalledWith(setArt(null));
});

test('should remove product from cart when "Delete" button is clicked', () => {
  store.dispatch = jest.fn();

  render(
    <Provider store={store}>
      <Modal />
    </Provider>
  );

  const deleteButton = screen.getByText('Delete');
  fireEvent.click(deleteButton);

  expect(store.dispatch).toHaveBeenCalledWith(removeProductFromCart([], 300125128));
});
