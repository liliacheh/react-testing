
export function BigGridIcon(){
    
    return(
        <svg fill="#000000" width="16" height="16" viewBox="0 0 8 8" xmlns="http://www.w3.org/2000/svg">
        <g strokeWidth="0"></g>
        <g strokeLinecap="round" strokeLinejoin="round"></g>
        <g>
          <path d="M0 0v2h2v-2h-2zm3 0v2h2v-2h-2zm3 0v2h2v-2h-2zm-6 3v2h2v-2h-2zm3 0v2h2v-2h-2zm3 0v2h2v-2h-2zm-6 3v2h2v-2h-2zm3 0v2h2v-2h-2zm3 0v2h2v-2h-2z"
          strokeLinecap="round" strokeLinejoin="round"/>
        </g>
      </svg>
      
    )
}

