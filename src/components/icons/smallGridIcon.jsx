export function SmallGridIcon(){
    return (
<svg fill="#000000" viewBox="0 0 7.00 7.00" xmlns="http://www.w3.org/2000/svg">
    <g strokeWidth="0"></g>
    <g strokeLinecap="round" strokeLinejoin="round"></g><g>
    <path d="M0 0v1h1v-1h-1zm2 0v1h1v-1h-1zm2 0v1h1v-1h-1zm2 0v1h1v-1h-1zm-6 2v1h1v-1h-1zm2 0v1h1v-1h-1zm2 0v1h1v-1h-1zm2 0v1h1v-1h-1zm-6 2v1h1v-1h-1zm2 0v1h1v-1h-1zm2 0v1h1v-1h-1zm2 0v1h1v-1h-1zm-6 2v1h1v-1h-1zm2 0v1h1v-1h-1zm2 0v1h1v-1h-1zm2 0v1h1v-1h-1z"></path>
    </g></svg>
)
}