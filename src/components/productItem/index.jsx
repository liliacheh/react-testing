import { useState, useEffect, useCallback} from "react"
import style from "./productItem.module.scss"
import Button from "../button";
import {FavIcon} from "../icons";
import PropTypes from 'prop-types';
import no_image from "../../img/no_image.png"
import { useDispatch, useSelector } from "react-redux";
import { getFavorites, setFavorites } from "../../redux/actions/favorites";
import { setArt } from "../../redux/actions/article";
import { setModalType } from "../../redux/actions/modal";
import { increaseCartItem, removeSingleItemFromCart } from "../../redux/actions/cart";


export default function ProductItem (props){
    const [isFavorite, setIsFavorite] = useState(false)
    const product = props.product
    const favorites = useSelector(state => state.favorites )
    const cart = useSelector (state => state.cart)
    const dispatch = useDispatch ()

    useEffect(()=> {
        dispatch(getFavorites)
        favorites.includes(product.article) 
        && setIsFavorite(true)
    },[favorites, product.article, dispatch])

    const countProduct = useCallback(() =>{
        const item = cart.find(item => item.id === product.article);
        return item ? item.quantity : 0;
    }, [cart, product.article])
 
    const [productNumber, setProductNumber] = useState(countProduct())

    useEffect(() => {
        setProductNumber((countProduct()))
    }, [cart, countProduct])

    const toggleIsFav = ()=> {
        isFavorite ? setIsFavorite(false) : setIsFavorite(true)
    }
   
    return(
            <a href='/' className={style.product}>
                <header className={style.product__header}>
                    <h3 className={style.product__name}>{product.name}</h3>
                  {props.type === 'delete'? 
                  <Button className={style.product__delBtn}
                  onClick={(e)=> {
                    e.preventDefault()
                    dispatch(setModalType('delete'))
                    dispatch(setArt(product.article))                                  
              }} title='Delete from cart'
                  >
                  </Button> : null}  
                </header>
                <div className={style.product__body}>
                    <div className={style.product__img}>
                        <img src={product.imgUrl || no_image} alt={product.name} />
                    </div>
                    <div className={style.product__descr}>
                    <div className={style.product__article}>{product.article}</div>
                        <div className={style.product__color}>{product.color}</div>
                        <div className={style.product__price}>{product.price} <span>₴</span>
                        </div>
                        {props.productNum && productNumber > 0
                            ?<div className={style.product__cartBtns}>
                            <Button className={style.product__minusBtn} onClick={(e) =>{
                                e.preventDefault()
                                dispatch(removeSingleItemFromCart(cart, product.article))
                            } } text={"-"}/>
                            <input className={style.product__itemQuantity}type="text" disabled value={countProduct()}/>
                            <Button className={style.product__plusBtn} onClick={(e) =>{
                                e.preventDefault()
                                dispatch(increaseCartItem(cart, product.article))}}
                                text={"+"}/>
                            </div>  : null}
                        <div className={style.product__btns}>
                            <Button className={style.product__favBtn}
                                onClick={(e)=> {  
                                e.preventDefault() 
                                dispatch(setFavorites(favorites, product.article))                           
                                toggleIsFav()}}
                                title='Add to favorites'
                            >
                                <FavIcon fill={isFavorite ? "black":"white"}/>
                            </Button>
                            {props.type ==='buy'?
                              <Button className={style.product__buyBtn}
                              onClick={(e)=> {
                                e.preventDefault()
                                dispatch(setModalType('buy'))
                                dispatch(setArt(product.article))                                 
                          }}
                              text={"Add to cart"}>
                              </Button>
                              :  null}
                        </div>
                    </div>
                </div>
                        </a >
)
}
    

ProductItem.propTypes = {
    type: PropTypes.string,
    productNum : PropTypes.bool,
    product: PropTypes.shape({
        name: PropTypes.string,
        article: PropTypes.number,
        imgUrl: PropTypes.string,
        color: PropTypes.string,
        price: PropTypes.number,
    })
}

ProductItem.defaultProps = {
    product: {
        imgUrl: no_image,
    },
}
