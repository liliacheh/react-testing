import style from './productList.module.scss';
import ProductItem from '../productItem';
import PropTypes from 'prop-types';
import { SmallGridIcon, BigGridIcon } from '../icons';
import { ViewContext } from '../../context/ViewContext';
import { useContext } from 'react';

export default function ProductList(props) {
  const { view, setView } = useContext(ViewContext);
  const setSmallGrid = () => {
    setView('smallGrid');
  };
  const setBigGrid = () => {
    setView('bigGrid');
  };
  const { products, type, productNum } = props;
  return (
    <div className={style.productList}>
      <div className={style.productList__container}>
        {type === 'buy' ? (
          <div className={style.productList__view}>
            <button onClick={setSmallGrid} title="Small tile">
              <SmallGridIcon />
            </button>
            <button onClick={setBigGrid} title="Large tile">
              <BigGridIcon />
            </button>
          </div>
        ) : null}
        <div className={view === 'bigGrid' ? style.productList__bigGrid : style.productList__smallGrid}>
          {products.map((product) => (
            <ProductItem key={product.article} product={product} productNum={productNum} type={type} />
          ))}
        </div>
      </div>
    </div>
  );
}

ProductList.propTypes = {
  product: PropTypes.array,
  productNum: PropTypes.bool,
  type: PropTypes.string,
};
