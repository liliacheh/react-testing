import { productsReducer } from "../reducers/products";
import { productsTypes } from "../types";
import { cleanup } from "@testing-library/react";

afterEach(cleanup);

const initialState = {
    loading: false,
    products: [],
    error: null,
  }
const mockedProducts = [{
    "name":"iPhone 11 Pro 64Gb (USED)",
    "price": 15999,
    "imgUrl": "https://pixel.com.ua/image/cache/catalog/11IPHONE/11/iphone11pro_midnight_green_main-228x228.jpg",
    "article": 300125126,
    "color" : "Midnight Green"
},
{
    "name":"iPhone 11 Pro 256Gb (USED)",
    "price": 18199,
    "imgUrl": "https://pixel.com.ua/image/cache/catalog/11IPHONE/11/iphone11pro_gold_main-228x228.jpg",
    "article": 300125127,
    "color" : "Gold "
}]
const error = "Sample error message"

test('should return default state when passed an empty action', () => {
    const result = productsReducer(undefined, { type : ''})
    expect(result).toEqual(initialState)
})
test('should return loading:true with "getProductsRequest" action', () => {
    const action = {type: productsTypes.GET_PRODUCTS_REQUESTED}
    const result = productsReducer(initialState, action)
    expect(result.loading).toBe(true)
})
test('should return products with "getProductsSuccess" action', () => {
    const action = {type: productsTypes.GET_PRODUCTS_SUCCESS, payload: mockedProducts}
    const result = productsReducer(initialState, action)
    expect(result.loading).toBe(false)
    expect(result.products).toEqual(mockedProducts)   
})
test ('should return error with "getProductsError" action', () => {
    const action = {type: productsTypes.GET_PRODUCTS_ERROR, payload: error}
    const result = productsReducer(initialState, action)
    expect(result.error).toBe(error)
    expect(result.loading).toBe(false)
})

