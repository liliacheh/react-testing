import { articleTypes } from "../types";
import { articleReducer } from "../reducers/article";
import { cleanup } from "@testing-library/react";

afterEach(cleanup);

test('should return default state when passed an empty action', ()=> {
    const result = articleReducer(undefined, { type : ''})
    expect(result).toEqual(null)
})
test('should return current article with "getArticle" action', () => {
    const action = {type: articleTypes.GET_ARTICLE}
    const result = articleReducer(300125128, action)
    expect(result).toEqual(300125128)
})
test('should set article with "setArt" action', () => {
    const action = {type: articleTypes.SET_ARTICLE, payload : 300125128}
    const result = articleReducer(null, action)
    expect(result).toEqual(300125128);
})