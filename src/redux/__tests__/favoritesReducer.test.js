import { favoritesTypes } from "../types";
import { favoritesReducer } from "../reducers/favorites";
import { cleanup } from "@testing-library/react";

afterEach(cleanup);

test('should return default state when passed an empty action', ()=> {
    const result = favoritesReducer(undefined, { type : ''})
    expect(result).toEqual([])
})
test('should return favorites with "getFav" action', () => {
    const action = {type: favoritesTypes.GET_FAVORITES, payload:[300125128, 300125127] }
    const result = favoritesReducer([], action)
    expect(result).toMatchObject([300125128, 300125127])
    })
test('should set favorites with "setFav" action', () => {
        const action = {type: favoritesTypes.SET_FAVORITES, payload : [300125128]}
        const result = favoritesReducer([], action)
        expect(result).toMatchObject([300125128]);
})