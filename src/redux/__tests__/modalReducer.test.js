import { modalReducer } from "../reducers/modal";
import { modalTypes } from "../types";
import { cleanup } from "@testing-library/react";

afterEach(cleanup);

test('should return default state when passed an empty action', ()=> {
    const result = modalReducer(undefined, { type : ''})
    expect(result.modalType).toEqual(null)
})
test('should return current modalType with "getModalType" action', () => {
    const action = {type: modalTypes.GET_MODAL_TYPE}
    const result = modalReducer('buy', action)
    expect(result).toMatch('buy')
})
test('should set modalType with "setModalType" action', () => {
    const action = {type: modalTypes.SET_MODAL_TYPE, payload : 'delete'}
    const result = modalReducer(null, action)
    expect(result.modalType).toMatch('delete')
})