import { cartQuantityTypes } from "../types";
import { cartQuantityReducer } from "../reducers/cartQuantity";
import { cleanup } from "@testing-library/react";

afterEach(cleanup);

test('should return default state when passed an empty action', ()=> {
    const result = cartQuantityReducer(undefined, { type : ''})
    expect(result).toEqual(null)
})
test('should return current cartQuantity with "getCartQuantity" action', () => {
    const action = {type: cartQuantityTypes.GET_CART_QUANTITY}
    const result = cartQuantityReducer(4, action)
    expect(result).toEqual(4)
})
test('should set cartQuantity with "setCartNum" action', () => {
    const action = {type: cartQuantityTypes.SET_CART_QUANTITY, payload : 5}
    const result = cartQuantityReducer(null, action)
    expect(result).toEqual(5);
})