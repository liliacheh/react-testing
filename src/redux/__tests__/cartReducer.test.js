import { cartReducer } from "../reducers/cart";
import { cartTypes } from "../types";
import { cleanup } from "@testing-library/react";

afterEach(cleanup);

const cart = [{id: 300125128, quantity: 2}, {id: 300125127, quantity: 1}]

test('should return default state when passed an empty action', ()=> {
    const result = cartReducer(undefined, { type : ''})
    expect(result).toEqual([])
})
test('should return cart with "getCart" action', () => {
    const action = {type: cartTypes.GET_CART, payload:cart }
    const result = cartReducer([], action)
    expect(result[0].id).toEqual(300125128)
    expect(result[1].quantity).toEqual(1)
})
test('should add item to cart with "addNewItemToCart" action', ()=> {
    const action = {type: cartTypes.ADD_TO_CART, payload:{id: 300125128, quantity: 1}}
    const result = cartReducer([], action)
    expect(result[0].id).toEqual(300125128)
    expect(result[0].quantity).toEqual(1)
})
test('should increase item num in cart with "increaseItemNumInCart" action', () => {
    const initialState = [{ id: 300125126, quantity: 2 }];
    const action = { type: cartTypes.INCREASE_ITEM_NUM_IN_CART, payload: { id: 300125126, quantity: 3 } };
    const result = cartReducer(initialState, action);
    expect(result[0].id).toEqual(300125126);
    expect(result[0].quantity).toEqual(3);
  });
test('should decrease item num in cart with "decreaseItemNumInCart" action', () => {
    const initialState = [{ id: 300125126, quantity: 2 }];
    const action = { type: cartTypes.INCREASE_ITEM_NUM_IN_CART, payload: { id: 300125126, quantity: 1 } };
    const result = cartReducer(initialState, action);
    expect(result[0].id).toEqual(300125126);
    expect(result[0].quantity).toEqual(1);
  });
test('should remove product from cart with "removeProductFromCart" action', ()=> {
    const action = {type: cartTypes.REMOVE_FROM_CART, payload: [{id: 300125127, quantity: 1}]}
    const result = cartReducer(cart, action);
    expect(result[0].id).toEqual(300125127);
    expect(result[0].quantity).toEqual(1)
})
test('should clear the cart with "clearCart" action', ()=> {
    const action = {type: cartTypes.CLEAR_CART}
    const result = cartReducer(cart, action)
    expect(result).toEqual([])
})