import { articleTypes } from "../types";

const initialState = null

export function articleReducer (state = initialState, action) {
    switch (action.type){
    case articleTypes.GET_ARTICLE:
        return state
    case articleTypes.SET_ARTICLE:
        return action.payload
    default: return state
    }
}