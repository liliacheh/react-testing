import { productsTypes } from "../types"

const initialState = {
    loading: false,
    products: [],
    error: null,
  }

   export function productsReducer(state = initialState, action) {
    switch(action.type) {
      case productsTypes.GET_PRODUCTS_REQUESTED:
        return {
          ...state, 
          loading:true,
          error: null
        }

      case productsTypes.GET_PRODUCTS_SUCCESS:
        return {
          ...state,
           loading:false,
           products:action.payload
          }

      case productsTypes.GET_PRODUCTS_ERROR:
        return {
          ...state,
           loading:false,
           error:action.payload}
        
      default:
        return state
    }
  }
  
  