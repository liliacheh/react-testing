import { modalTypes } from "../types"

export function setModalType(type){
    return {
        type: modalTypes.SET_MODAL_TYPE,
        payload : type
    }
}

export function getModalType(){
    return {
        type: modalTypes.GET_MODAL_TYPE,
    }
}