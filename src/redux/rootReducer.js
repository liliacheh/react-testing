import { combineReducers } from "redux"
import { productsReducer as products } from "./reducers/products"
import { modalReducer as modalType } from "./reducers/modal"
import {cartReducer as cart} from './reducers/cart'
import { articleReducer as article } from "./reducers/article"
import { favoritesReducer as favorites } from "./reducers/favorites"
import {cartQuantityReducer as cartQuantity} from "./reducers/cartQuantity"

export const rootReducer = combineReducers ({
    products,
    modalType,
    cart,
    article,
    favorites,
    cartQuantity
  })
  