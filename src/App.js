import { RouterProvider } from 'react-router-dom';
import './App.scss';
import { router } from './router';
import { ViewContext } from './context/ViewContext';
import { useState } from 'react';

const App = () => {
const [view, setView] = useState('smallGrid')
return (
<ViewContext.Provider value={{view, setView}}>
<RouterProvider router={router} />; 
</ViewContext.Provider>
)
}
export default App;
